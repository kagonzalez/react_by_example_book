module.exports = {
    componentWillMount(){
        this.intervals = [];
        console.log('will mount: set interval');
    },
    setInterval(){
        this.intervals.push(setInterval.apply(null, arguments));
    },
    componentWillUnmount(){
        //this.intervals.map(clearInterval);
        this.intervals.map((_int)=>{clearInterval(_int);});
        console.log('will umount');
    }
};
